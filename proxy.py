from flask import Flask, request, redirect, Response, jsonify
import requests
import json

app = Flask(__name__)

EASYJET = 'http://easyjet.com/ejcms/cache15m/api/lowestfares/get'
WIZZAIR = 'https://be.wizzair.com/%s/Api/search/timetable'
RYANAIR = 'https://www.ryanair.com/api/farfnd/3/roundTripFares/%s/%s/cheapestPerDay'
WIZZAIR_API = 'https://wizzair.com/static_fe/metadata.json'
WIZZAIR_MAP = 'https://be.wizzair.com/%s/Api/asset/map?languageCode=pl-pl'
RYANAIR_MAP = 'https://www.ryanair.com/api/locate/3/airports?market=pl-pl'

@app.route('/flights/data/api', methods=['GET'])
def api_proxy():
    resp = requests.get(WIZZAIR_API)
    response = app.response_class(
        response=json.dumps(resp.json()),
        mimetype='application/json'
    )
    return response

@app.route('/flights/data/map/wizzair', methods=['GET'])
def wizzair_map():
    resp = requests.get(WIZZAIR_MAP % request.values['apiKey'])

    response = app.response_class(
        response=json.dumps(resp.json()),
        mimetype='application/json'
    )
    return response

@app.route('/flights/data/map/ryanair', methods=['GET'])
def ryanair_map():
    resp = requests.get(RYANAIR_MAP)
    response = app.response_class(
        response=json.dumps(resp.json()),
        mimetype='application/json'
    )
    return response

@app.route('/flights/data/ryanair', methods=['GET'])
def proxy_ryanair():
    data = {
        'outboundMonthOfDate': request.values['start_date'],
        'inboundMonthOfDate': request.values['end_date'],
        'market': 'pl-pl',
    }
    request_path = RYANAIR % (request.values['from'], request.values['to'])
    resp = requests.get(request_path, params=data)
    response = app.response_class(
        response=json.dumps(resp.json()),
        mimetype='application/json'
    )
    return response

@app.route('/flights/data/easyjet', methods=['GET'])
def proxy_easyjet():
    print("EASYJET")
    data = {
        'destinationIata': request.values['to'],
        'originIata': request.values['from'],
        'displayCurrencyId': '54',
        'languageCode': 'pl-PL',
        'startDate': request.values['start_date'],
    }
    request_path = EASYJET
    resp = requests.get(request_path, params=data)
    response = app.response_class(
        response=json.dumps(resp.json()),
        mimetype='application/json'
    )
    return response


@app.route('/flights/data/wizzair', methods=['GET'])
def proxy_WIZZAIR():
    data = {
        'flightList': [
            {
                'departureStation': request.values['from'],
                'arrivalStation': request.values['to'],
                'from': request.values['start_date'],
                'to': request.values['end_date'],
            },
            {
                'departureStation': request.values['to'],
                'arrivalStation': request.values['from'],
                'from': request.values['start_date'],
                'to': request.values['end_date'],
            }
        ],
        'priceType': request.values['wdc'],
        'adultCount': 1,
        'childCount': 0,
        'infantCount': 0
    }
    data = str(data).replace('u\'', '\'')
    request_path = WIZZAIR % request.values['api_key']
    headers = {
        'accept': 'application/json, text/plain, */*',
        'Connection': '',
        'content-type': 'application/json;charset=UTF-8',
        'x-requestverificationtoken': '0276b0ef9c85413780e08e62db3e8329',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
        'origin': 'https://wizzair.com'
    }
    resp = requests.post(request_path, data=data, headers=headers)
    response = app.response_class(
        response=json.dumps(resp.json()),
        mimetype='application/json'
    )
    return response

if __name__ == '__main__':
    app.run(host='localhost', port=6666, debug=True)
