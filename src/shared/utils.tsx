import { Airport, MonthValue, Routes } from "./interfaces";

export const formatDate = (date: Date) => date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);

export const getRyanAirResults = (res: any, from: string, to: string, rates: {[key: string]: number}, currency: string) =>
    res.filter((obj: any) => !obj.unavailable)
        .map((obj: any) => ({
            airline: 'RyanAir',
            from: from,
            to: to,
            price: obj.price.currencyCode in rates ? Math.floor(parseInt(obj.price.value) * 1 / rates[obj.price.currencyCode]) : parseInt(obj.price.value),
            currency: obj.price.currencyCode in rates ? currency : obj.price.currencyCode,
            date: obj.day,
        }));

export const getEasyJestResults = (res: any, from: string, to: string, month: MonthValue, rates: { [key: string]: number }, currency: string) => {
    if (res.data.months[0] && res.data.months[0].month !== month.month + 1) {
        return []
    }
    return (res.data.months[0] || { days: [] }).days
        .filter((obj: any) => !!obj.lowestFare)
        .map((obj: any) => ({
            airline: 'EasyJet',
            from: from,
            to: to,
            price: 'PLN' in rates ? Math.floor(parseInt(obj.lowestFare) * 1 / rates['PLN']) : parseInt(obj.lowestFare),
            currency,
            date: formatDate(new Date(month.year, month.month, obj.day)),
        }));
    }
export const getWizzAirResults = (res: any, rates: { [key: string]: number }, currency: string) =>
    res.filter((obj: any) => !["noData", "soldOut", "checkPrice"].includes(obj.priceType))
        .map((obj: any) => ({
            airline: 'WizzAir',
            from: obj.departureStation,
            to: obj.arrivalStation,
            price: obj.price.currencyCode in rates ? Math.floor(parseInt(obj.price.amount) * 1 / rates[obj.price.currencyCode]) : parseInt(obj.price.amount),
            currency: obj.price.currencyCode in rates ? currency : obj.price.currencyCode,
            date: obj.departureDate.split('T')[0],
        }))

export const getEasyJetData = (from: string, to: string, month: MonthValue) => {
    const minDate = new Date();
    const startDate = new Date(month.year, month.month, 1)
    return {
        to,
        from,
        start_date: formatDate(startDate < minDate ? minDate : startDate),
    };
}

export const getWizzAirData = (from: string, to: string, month: MonthValue, wdc: boolean, apiKey: string) => {
    const minDate = new Date();
    const fromDateStart = new Date(month.year, month.month, 1);
    const fromDateEnd = new Date(month.year, month.month + 1, 0);
    const formatedStartDate = formatDate(fromDateStart < minDate ? minDate : fromDateStart)
    const formatedEndDate = formatDate(fromDateEnd < minDate ? minDate : fromDateEnd)
    return {
        from,
        to,
        start_date: formatedStartDate,
        end_date: formatedEndDate,
        wdc: wdc ? 'wdc' : 'regular',
        api_key: apiKey,
    }
}
export const getRyanData = (from: string, to: string, month: MonthValue) => {
    const minDate = new Date();
    const startDate = new Date(month.year, month.month, 1)
    const formatedDate = formatDate(startDate < minDate ? minDate : startDate)
    return {
        from,
        to,
        start_date: formatedDate,
        end_date: formatedDate,
    }
}

export const checkConnection = (routes: { [key: string]: string[] }, from: string, to: string) => ((routes[from] || []).includes(to));

export const shadowCSS = {
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4
}

interface RyanAirResponse {
    iataCode: string;
    name: string;
    routes: string[];
    countryCode: string;
    coordinates: {latitude: number; longitude: number};
}

interface WizzAirReposnse {
    cities: {
        connections: {
            iata: string;
            operationStartDate: string;
        }[];
        countryCode: string;
        countryName: string;
        iata: string;
        latitude: number;
        longitude: number;
        shortName: string;
    }[];
}

export const getAirportsFromWizz = (data: WizzAirReposnse): Airport[] =>
    data.cities.map(city => ({
        name: city.shortName,
        id: city.iata,
        country: city.countryCode,
        coords: [city.latitude, city.longitude]
    }))

export const getAirportsFromRyan = (data: RyanAirResponse[]): Airport[] =>
    data.map(obj => ({
        name: obj.name,
        country: obj.countryCode,
        id: obj.iataCode,
        coords: [obj.coordinates.latitude, obj.coordinates.longitude],
    }))

export const getRyanAirRoutes = (data: RyanAirResponse[]): Routes =>
    data.reduce((prev, next) => ({
        ...prev,
        [next.iataCode]: next.routes
            .filter(obj => obj.startsWith('airport'))
            .map(obj => obj.replace('airport:', ''))
    }), {})


export const getWizzAirRoutes = (data: WizzAirReposnse): Routes =>
    data.cities.reduce((prev, next) => ({
        ...prev, [next.iata]:
        next.connections.map(obj => obj.iata)
    }), {})
