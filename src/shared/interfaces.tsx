export interface MonthValue {
    title: string;
    month: number;
    year: number;
    value: number;
    text: string;
}

export interface Airport {
    name: string;
    id: string;
    country: string;
    coords: number[];
};

export interface AirportSelect {
    name: string;
    id: string;
    country: string;
    text: string;
};

export interface Result {
    airline: string;
    from: string;
    to: string;
    price: number;
    currency: string;
    date: string;
}

export interface Day {
    flights: Result[];
    flightsReturn: Result[];
    date: Date;
    dateFormated: string;
    disabled: boolean;
    lowestPrice: string;
    lowestPriceReturn: string;
}

export interface SearchFormState {
    startDate: MonthValue;
    fromAirports: Airport[];
    destinationAirports: Airport[];
    wizzAirAPI: string;
    wdc: boolean;
    currency: string;
}

export interface SelectOption {
    id: string;
    text: string;
    name?: string;
}

export interface InfoState {
    info: string;
    showInfo: boolean;
}

export interface ResultsState {
    from: Result[];
    return: Result[];
    loading: boolean;
    id: number;
    total: number;
    done: number;
    showResults: boolean;
}

export interface State {
    form: SearchFormState;
    results: ResultsState;
    info: InfoState;
    routes: RoutesState;
    rates: Rates
}

export interface Rates {
    rates: {[key: string]: number };
}

export interface Routes {
    [key: string]: string[];
}

export interface RoutesState {
    airports: Airport[];
    wizzair: Routes;
    ryanair: Routes;
    failure: boolean;
}
