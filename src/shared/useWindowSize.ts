import { useState, useEffect } from 'react';

export function useWindowSize() {

  function getSize() {
    return {
      width: window.innerWidth,
      height: window.innerHeight,
    };
  }

  const [windowSize, setWindowSize] = useState(getSize);

  useEffect(() => {
    
    function handleResize() {
      const size = getSize()
      if (size.width >= 500) {
        setWindowSize(size);
      }
    }

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);
  return windowSize;
}