import { Airport } from "../shared/interfaces";
import { addInfoNotification } from "./infoActions";
import axios from 'axios';
import { getWizzAirMap } from "./routesActions";

export const UPDATE_FORM = 'UPDATE_FORM';
export const ADD_AIRPORT = 'ADD_AIRPORT';
export const ADD_COUNTRY = 'ADD_COUNTRY';
export const REMOVE_AIRPORT = 'REMOVE_AIRPORT';
export const CLEAR_AIRPORTS = 'CLEAR_AIRPORTS';

export const updateForm = (selector: string, value: any) => ({
    type: UPDATE_FORM,
    selector,
    value,
})

const updateAirports = (selectorAirports: string, value: Airport) => ({
    type: ADD_AIRPORT,
    selectorAirports,
    value,
})

export const addAirport = (selectorAirports: string, value: Airport) => ((dispatch: any, getState: any) => {
    dispatch(updateAirports(selectorAirports, value));
    dispatch(addInfoNotification(`Added ${value.name} ${value.id}`))
})

export const updateContries = (selectorAirports: string, value: string) => ({
    type: ADD_COUNTRY,
    selectorAirports,
    value,
})

export const addCountry = (selectorAirports: string, value: string) => ((dispatch: any, getState: any) => {
    dispatch(updateContries(selectorAirports, value));
    dispatch(addInfoNotification(`Added ${value}`))
})

export const removeAirport = (selectorAirports: string, value: number) => ({
    type: REMOVE_AIRPORT,
    selectorAirports,
    value,
})

export const clearAirports = (selectorAirports: string) => ({
    type: CLEAR_AIRPORTS,
    selectorAirports,
})

export const getWizzAirApi = () => (dispatch: any, getState: any) => {
    axios.get<{ apiUrl: string }>('/flights/data/api')
        .then(res => {
            const api = res.data.apiUrl.split('/').slice(-2)[0]
            dispatch(updateForm('wizzAirAPI', api))
            dispatch(getWizzAirMap(api))
        })
        .catch(_ => console.log('WizzAir not working'));
}
