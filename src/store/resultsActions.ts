import { Airport, Result, MonthValue, Rates, State } from "../shared/interfaces";
import { getRyanAirResults, getWizzAirResults, getWizzAirData, getRyanData, checkConnection } from "../shared/utils";
import axios from 'axios';

export const SAVE_RESULT = 'SAVE_RESULT';
export const START_GET_DATA = 'START_GET_DATA';
export const CLEAR_RESULTS = 'CLEAR_RESULTS';
export const STOP_LOADING = 'STOP_LOADING';
export const UPDATE_TOTAL = 'UPDATE_TOTAL';
export const FAILURE_GETTING_DATA = 'FAILURE_GETTING_DATA'
export const CLOSE_RESULTS = 'CLOSE_RESULTS'

export const stopLoading = () => ({
    type: STOP_LOADING
})

export const closeResults = () => ({
    type: CLOSE_RESULTS
})

export const clearResults = () => ({
    type: CLEAR_RESULTS
})

export const saveResult = (selector: 'from' | 'return', results: Result[], id: number) => ({
    type: SAVE_RESULT,
    value: results,
    selector,
    id,
})

export const startGetData = (id: number) => ({
    type: START_GET_DATA,
    id
})

export const updateTotal = (id: number, total: number) => ({
    type: UPDATE_TOTAL,
    id,
    total,
})

const failureGettingData = (id: number, total: number) => ({
    type: FAILURE_GETTING_DATA,
    id,
    total,
})

const getWizzAirMonthlyFlights = (from: string, to: string, month: MonthValue, wdc: boolean, apiKey: string, id: number, rates: Rates, currency: string,  dispatch: any) => {
    const apiLink = `/flights/data/wizzair`;
    const data = getWizzAirData(from, to, month, wdc, apiKey);
    dispatch(updateTotal(id, 2));
    axios.get(apiLink, {params: data})
        .then((res: any) => {
            dispatch(saveResult('from', getWizzAirResults(res.data.outboundFlights, rates.rates, currency), id))
            dispatch(saveResult('return', getWizzAirResults(res.data.returnFlights, rates.rates, currency), id))
        })
        .catch(err => dispatch(failureGettingData(id, 2)));
}

// const getEasyJetMonthlyFlights = (from: string, to: string, month: MonthValue, id: number, rates: Rates, currency: string, dispatch: any) => {
//     const apiLink  = `/flights/data/easyjet`;
//     const data = getEasyJetData(from, to, month);
//     dispatch(updateTotal(id, 2));
//     axios.get(apiLink, { params: data })
//         .then((res: any) => dispatch(saveResult('from', getEasyJestResults(res, from, to, month, rates.rates, currency), id)))
//         .catch(err => dispatch(failureGettingData(id, 1)));
//     axios.get(apiLink, { params: { ...data, to: from, from: to} })
//         .then(res => dispatch(saveResult('return', getEasyJestResults(res, to, from, month, rates.rates, currency), id)))
//         .catch(err => dispatch(failureGettingData(id, 1)));
// }

const getRyanAirMonthlyFlights = (from: string, to: string, month: MonthValue, id: number, rates: Rates, currency: string, dispatch: any) => {
    const apiLink = `/flights/data/ryanair`;
    const data = getRyanData(from, to, month);
    dispatch(updateTotal(id, 2));
    axios.get(apiLink, { params: data })
        .then((res: any) => {
            dispatch(saveResult('from', getRyanAirResults(res.data.outbound.fares, from, to, rates.rates, currency), id));
            dispatch(saveResult('return', getRyanAirResults(res.data.inbound.fares, to, from, rates.rates, currency), id));
        })
        .catch(err => dispatch(failureGettingData(id, 2)));
}

export const submit = () => ((dispatch: any, getState: any) => {
    const state: State = getState();
    const newId = state.results.id + 1;
    dispatch(startGetData(newId));
    let anyConnections = false;
    state.form.fromAirports.forEach(((airport: Airport) => {
        state.form.destinationAirports.forEach((airportTo: Airport) => {
            const currentRoutes =
                (state.routes.wizzair[airport.id] || []).includes(airportTo.id) ||
                (state.routes.ryanair[airport.id] || []).includes(airportTo.id);
                // (ROUTES_EASYJET[airport.id] || []).includes(airportTo.id);
            anyConnections = currentRoutes ? true : anyConnections;
            if (currentRoutes) {
                checkConnection(state.routes.wizzair, airport.id, airportTo.id) &&
                    getWizzAirMonthlyFlights(airport.id, airportTo.id, state.form.startDate, state.form.wdc, state.form.wizzAirAPI, newId, state.rates, state.form.currency, dispatch);
                checkConnection(state.routes.ryanair, airport.id, airportTo.id) &&
                    getRyanAirMonthlyFlights(airport.id, airportTo.id, state.form.startDate, newId, state.rates, state.form.currency, dispatch);
                // checkConnection(ROUTES_EASYJET, airportTo.id, airport.id) &&
                //     getEasyJetMonthlyFlights(airport.id, airportTo.id, state.form.startDate, newId, state.rates, state.form.currency, dispatch);
            }
        })
    }));
    if (!anyConnections) {
        dispatch(stopLoading());
    }
})
