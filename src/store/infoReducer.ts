import * as actionTypes from './infoActions';
import { InfoState } from '../shared/interfaces';

const initialState: InfoState = {
    info: '',
    showInfo: false,
}

interface Action {
    type: string;
    info: string;
}

const infoReducer = (state: InfoState = initialState, action: Action): InfoState => {
    switch (action.type) {
        case actionTypes.ADD_INFO:
            return { ...state, showInfo: true, info: action.info };
        case actionTypes.CLOSE_INF0:
            return { ...state, showInfo: false, info: ''};
        default:
            return state;
    }
}

export default infoReducer;
