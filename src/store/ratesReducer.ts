import * as actionTypes from './ratesActions';
import { Rates } from '../shared/interfaces';
import { RATES_EUR, RATES_PLN } from '../shared/data';

const initialState: Rates = {
  rates: {},
};

interface Action {
    type: string;
    rates: {[key: string]: number};
}

const ratesReducer = (state: Rates = initialState, action: Action): Rates => {
    switch (action.type) {
        case actionTypes.SAVE_RATES:
            return { ...state, rates: action.rates };
        case actionTypes.CHANGE_EURO:
            return { ...state, rates: RATES_EUR };
        case actionTypes.CHANGE_PLN:
            return { ...state, rates: RATES_PLN };
        default:
            return state;
    }
}

export default ratesReducer;
