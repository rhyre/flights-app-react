// import axios from 'axios';
export const SAVE_RATES = 'SAVE_RATES';
export const CHANGE_EURO = 'CHANGE_EURO';
export const CHANGE_PLN = 'CHANGE_PLN';

export const saveRates = (rates: {[key: string]: number}) => ({
    type: SAVE_RATES,
    rates
})

export const saveEur = () => ({
    type: CHANGE_EURO,
})

export const savePln = () => ({
    type: CHANGE_PLN,
})

export const getRates = (currency: string) => (dispatch: any, getState: any) => {
    if (currency === 'EUR') {
        dispatch(saveEur());
    } else {
        dispatch(savePln());
    }
    // NO API KEY
    // axios.get('https://api.exchangeratesapi.io/latest', { params: {base: currency} })
    //     .then((res: any) => {
    //         dispatch(saveRates(res.data.rates));
    //     })
    //     .catch(err => console.log('error getting currency exchange'));
}
