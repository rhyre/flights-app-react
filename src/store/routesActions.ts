import { Airport, Routes } from "../shared/interfaces";
import { getWizzAirRoutes, getRyanAirRoutes, getAirportsFromWizz, getAirportsFromRyan } from "../shared/utils";
import axios from 'axios';

export const SAVE_RESULT_WIZZ = 'SAVE_RESULT_WIZZ';
export const SAVE_RESULT_RYAN = 'SAVE_RESULT_RYAN';
export const FAILURE_LOADING_DATA = 'FAILURE_LOADING_DATA'
export const UPDATE_AIRPORTS = 'UPDATE_AIRPORTS'

const saveResultRyan = (results: Routes) => ({
    type: SAVE_RESULT_RYAN,
    value: results,
})

const failureGettingData = () => ({
    type: FAILURE_LOADING_DATA,
})

const saveResultWizz = (results: Routes) => ({
    type: SAVE_RESULT_WIZZ,
    value: results,
})

const updateAirports = (results: Airport[]) => ({
    type: UPDATE_AIRPORTS,
    airports: results,
})

export const getWizzAirMap = (apiKey: string) => (dispatch: any) => {
    const apiLink = `/flights/data/map/wizzair`;
    axios.get(apiLink, {params: {apiKey}})
        .then((res: any) => {
            dispatch(saveResultWizz(getWizzAirRoutes(res.data)))
            dispatch(updateAirports(getAirportsFromWizz(res.data)))
        })
        .catch(err => dispatch(failureGettingData()));
}

export const getRyanAirMap = () => (dispatch: any) => {
    const apiLink = `/flights/data/map/ryanair`;
    axios.get(apiLink)
        .then((res: any) => {
            dispatch(saveResultRyan(getRyanAirRoutes(res.data)))
            dispatch(updateAirports(getAirportsFromRyan(res.data)))
        })
        .catch(err => dispatch(failureGettingData()));
}
