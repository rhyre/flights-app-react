import * as actionTypes from './resultsActions';
import { ResultsState, Airport } from '../shared/interfaces';

const initialState: ResultsState = {
    from: [],
    return: [],
    loading: false,
    id: 0,
    total: 0,
    done: 0,
    showResults: false,
}

interface Action {
    type: string;
    selector: 'from' | 'return';
    value: Airport[];
    total: number;
    id: number;
};

const resultsReducer = (state: ResultsState = initialState, action: Action): ResultsState => {
    switch (action.type) {
        case actionTypes.FAILURE_GETTING_DATA:
            return action.id === state.id ? { ...state, done: state.done + action.total } : state;
        case actionTypes.UPDATE_TOTAL:
            return { ...state, total: state.total + action.total };
        case actionTypes.STOP_LOADING:
            return { ...state, loading: false };
        case actionTypes.START_GET_DATA:
            return { ...state, loading: true, id: action.id, done: 0, total: 0, from: [], return: [], showResults: true };
        case actionTypes.CLEAR_RESULTS:
            return { ...state, from: [], return: []};
        case actionTypes.SAVE_RESULT:
            return action.id === state.id ? { ...state, [action.selector]: [...state[action.selector], ...action.value], loading: false, done: state.done + 1 } : state
        case actionTypes.CLOSE_RESULTS:
            return { ...state, showResults: false};
        default:
            return state;
    }
}

export default resultsReducer;
