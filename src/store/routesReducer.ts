import * as actionTypes from './routesActions';
import { RoutesState, Routes, Airport } from '../shared/interfaces';
import { AIRPORTS, RYANAIR_ROUTES, WIZZAIR_ROUTES } from '../shared/data';

const initialState: RoutesState = {
    airports: AIRPORTS,
    wizzair: WIZZAIR_ROUTES,
    ryanair: RYANAIR_ROUTES,
    failure: false,
}

const routesReducer = (state: RoutesState = initialState, action: {type: string, value: Routes, airports: Airport[]}): RoutesState => {
    switch (action.type) {
        case actionTypes.SAVE_RESULT_WIZZ:
            return { ...state, wizzair: action.value}
        case actionTypes.SAVE_RESULT_RYAN:
            return { ...state, ryanair: action.value}
        case actionTypes.FAILURE_LOADING_DATA:
            return {...state, failure: true}
        case actionTypes.UPDATE_AIRPORTS:
            return {...state, airports: action.airports.reduce((prev, next) => {
                if (prev.find(obj => obj.id === next.id)) {
                    return prev
                }
                return [...prev, next]
            }, state.airports).sort((a, b) => (a.name > b.name ? 1 : -1))}
        default:
            return state;
    }
}

export default routesReducer;
