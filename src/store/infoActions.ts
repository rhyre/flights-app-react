export const ADD_INFO = 'ADD_INFO';
export const CLOSE_INF0 = 'CLOSE_INF0';

export const addInfoNotification = (info: string) => ({
    type: ADD_INFO,
    info
})

export const closeNotification = () => ({
    type: CLOSE_INF0,
})
