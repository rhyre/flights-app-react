import * as actionTypes from './formActions';
import { Airport, SearchFormState } from '../shared/interfaces';
import { AIRPORTS, MONTHS } from '../shared/data';

const initialState: SearchFormState = {
    startDate: MONTHS[0],
    fromAirports: [],
    destinationAirports: [],
    wdc: true,
    wizzAirAPI: '',
    currency: 'PLN',
}

interface Action {
    type: string;
    selector: 'startDate' | 'wizzAirAPI' | 'wdc';
    selectorAirports: 'fromAirports' | 'destinationAirports';
    value: any;
}

const formReducer = (state: SearchFormState = initialState, action: Action): SearchFormState => {
    switch (action.type) {
        case actionTypes.UPDATE_FORM:
            return { ...state, [action.selector]: action.value };
        case actionTypes.ADD_AIRPORT:
            if (state[action.selectorAirports].find((obj: Airport) => obj.id === action.value.id)) {
                return state;
            }
            return {
                ...state,
                [action.selectorAirports]: [...state[action.selectorAirports], action.value]
            }
        case actionTypes.REMOVE_AIRPORT:
            const nextAirports = [...state[action.selectorAirports]];
            nextAirports.splice(action.value, 1);
            return {
                ...state,
                [action.selectorAirports]: nextAirports
            }
        case actionTypes.CLEAR_AIRPORTS:
            return {
                ...state,
                [action.selectorAirports]: [],
            }
        case actionTypes.ADD_COUNTRY:
            return {
                ...state,
                [action.selectorAirports]: [
                    ...state[action.selectorAirports],
                    ...AIRPORTS.filter((obj: Airport) =>
                        obj.country.toLowerCase() === action.value.toLowerCase() &&
                        !state[action.selectorAirports].find(airportSelected => airportSelected.id === obj.id))]
            }
        default:
            return state;
    }
}

export default formReducer;
