import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import formReducer from './formReducer';
import resultsReducer from './resultsReducer';
import thunk from 'redux-thunk';
import infoReducer from './infoReducer';
import ratesReducer from './ratesReducer';
import routesReducer from './routesReducer';


const rootReducer = combineReducers(
    {
        form: formReducer,
        results: resultsReducer,
        info: infoReducer,
        rates: ratesReducer,
        routes: routesReducer,
    }
);
const configureStore = () => {
    return createStore(rootReducer, compose(applyMiddleware(thunk)));
}
export default configureStore;
