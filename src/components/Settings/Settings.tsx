import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../shared/interfaces';
import { FormControlLabel, Checkbox, FormControl, InputLabel, Select, MenuItem, Paper } from '@material-ui/core';
import { updateForm } from '../../store/formActions';
import './Settings.css';
import { getRates } from '../../store/ratesActions';

export const Settings = () => {
    const formControls = useSelector((state: State) => state.form)
    const dispatch = useDispatch();
    const updateCurrency = (val: string) => {
        dispatch(updateForm('currency', val))
        dispatch(getRates(val));
    };
    return (
        <Paper className="settings-container">
            <FormControlLabel
                label="WizzAir Discount Club"
                control={
                    <Checkbox
                        color="primary"
                        checked={formControls.wdc}
                        onChange={() => dispatch(updateForm('wdc', !formControls.wdc))}
                        value="check" />
                }
            />
            <FormControl>
                <InputLabel id="currency">Currency</InputLabel>
                <Select
                    labelId="currency"
                    value={formControls.currency}
                    onChange={(event: any) => updateCurrency(event.target.value)}
                >
                    <MenuItem value={'PLN'}>PLN</MenuItem>
                    <MenuItem value={'EUR'}>EUR</MenuItem>
                </Select>
            </FormControl>
        </Paper>
    );
}

export default Settings;
