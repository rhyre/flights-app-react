import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import './Home.css';
import SearchFormControls from './SearchFormControls/SearchFormControls';
import Results from './Results/Results';
import { getWizzAirApi } from '../../store/formActions';
import LoadingBar from './LoadingBar/LoadingBar';
import ResultsModal from './Results/ResultsModal';
import SnackBarInfo from './SnackBarInfo/SnackBarInfo';
import { getRates } from '../../store/ratesActions';
import { Tabs, Tab } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Settings from '../Settings/Settings';
// import MapTable from '../Map/MapTable';
import { useWindowSize } from '../../shared/useWindowSize';
import { getRyanAirMap } from '../../store/routesActions';
// const Map = React.lazy(() => import('../Map/Map'));


interface TabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`nav-tabpanel-${index}`}
            aria-labelledby={`nav-tab-${index}`}
            className="form-box"
            {...other}
        >
            {value === index && <Box className="form-box">{children}</Box>}
        </Typography>
    );
}

function a11yProps(index: any) {
    return {
        id: `nav-tab-${index}`,
        'aria-controls': `nav-tabpanel-${index}`,
    };
}

interface LinkTabProps {
    label?: string;
    href?: string;
}

function LinkTab(props: LinkTabProps) {
    return (
        <Tab
            component="a"
            onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
                event.preventDefault();
            }}
            {...props}
        />
    );
}



export const Home = () => {
    const [value, setValue] = React.useState(0);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getRates('PLN')); // TODO: Change to use settings val
        dispatch(getWizzAirApi());
        dispatch(getRyanAirMap());
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };
    const { height, width } = useWindowSize();
    const isMobile = width <= 500;
    return (
        <div className="search-container">
            <LoadingBar/>
            <div className="form-container" style={{ height: height + 'px' }}>
                <AppBar
                    elevation={1}
                    color="default"
                    position="static">
                    <Tabs
                        variant="fullWidth"
                        value={value}
                        onChange={handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        aria-label="nav tabs example"
                        >
                        <LinkTab label="Search" href="/search" {...a11yProps(0)} />
                        <LinkTab label="Settings" href="/settings" {...a11yProps(2)} />
                    </Tabs>
                </AppBar>
                <TabPanel value={value} index={0}>
                    <SearchFormControls/>
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <Settings />
                </TabPanel>
            </div>
            {isMobile ?
                <ResultsModal/> :
                <div className="results-container">
                    <Results/>
                </div>
            }
            <SnackBarInfo/>
        </div>
        );
}

export default Home;
