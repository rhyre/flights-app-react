import React, { useState } from 'react';
import { Result } from '../../../../shared/interfaces';
import { Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import './ResultList.css';

interface props { results: Result[] | null, label: string };

export const ResultList = React.memo(({ results, label }: props) => {
    const [sorting, setSorting] = useState('');
    const getTicketLink = (flight: Result) => flight.airline === 'WizzAir' ?
            `https://wizzair.com/#/booking/select-flight/${flight.from}/${flight.to}/${flight.date.split('T')[0]}/null/1/0/0/null` :
            `https://www.ryanair.com/pl/pl/trip/flights/select?adults=1&dateOut=${flight.date.split('T')[0]}&originIata=${flight.from}&destinationIata=${flight.to}`;
    const sortedResults = results ? results.sort((a: any, b: any) => a[sorting || 'date'] > b[sorting || 'date'] ? 1 : -1) : [];
    return (
    <div className="results-table-container">
        <div className="results-table__header">{label}</div>
        <Table size="small">
            <TableHead>
                <TableRow>
                    <TableCell className="table-data">Airline</TableCell>
                    <TableCell className="table-data sort" align="right" onClick={() => setSorting('from')}>From</TableCell>
                    <TableCell className="table-data sort" align="right" onClick={() => setSorting('to')}>To</TableCell>
                    <TableCell className="table-data sort" align="right" onClick={() => setSorting('price')}>Price</TableCell>
                    <TableCell className="table-data" align="right">Ticket</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {sortedResults.map(row => (
                    <TableRow key={JSON.stringify(row)} className="table-row">
                        <TableCell className="table-data" scope="row">{row.airline}</TableCell>
                        <TableCell className="table-data" align="right">{row.from}</TableCell>
                        <TableCell className="table-data" align="right">{row.to}</TableCell>
                        <TableCell className="table-data" align="right">{row.price} {row.currency}</TableCell>
                        <TableCell className="table-data" align="right">{row.airline !== 'EZ' ? <a href={getTicketLink(row)} className="link">Ticket</a> : '-'}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    </div>
    );
})

export default ResultList;
