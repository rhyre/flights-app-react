import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../../shared/interfaces';
import { closeResults } from '../../../store/resultsActions';
import Results from './Results';
import './ResultsModal.css';


export default function ResultsModal() {
    const open = useSelector((state: State) => state.results.showResults)
    const dispatch = useDispatch();
    const handleClose = () => {
        dispatch(closeResults());
    };

    return (
        <Dialog fullScreen open={open} onClose={handleClose}
        >
            <AppBar>
                <Toolbar>
                    <Typography variant="h6">
                        Results
                    </Typography>
                    <IconButton edge="start" style={{marginLeft: 'auto'}} color="inherit" onClick={handleClose} aria-label="close">
                        <CloseIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <div className="results-wrapper">
                <Results />
            </div>
        </Dialog>
    );
}
