import React from 'react';
import { State, Result } from '../../../../shared/interfaces';
import './ResultCalendar.css';
import { MONTHS } from '../../../../shared/data';
import { formatDate } from '../../../../shared/utils';
import { useSelector, useDispatch } from 'react-redux';
import { Button } from '@material-ui/core';
import { updateForm } from '../../../../store/formActions';
import { submit } from '../../../../store/resultsActions';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import { useDrag } from 'react-use-gesture'

interface props { daySelected: any };

const oneDay = 86400000; // in seconds
const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']

const getDayFlights = (results: Result[], date: string) : [Result[], string] => {
    const flights = results.filter(flight => flight.date === date);
    const cheapestFlight = flights.sort((a, b) => (a.price - b.price))[0];
    const lowestPrice = cheapestFlight ? `${Math.floor(cheapestFlight.price)} ${cheapestFlight.currency}` : '';
    return [flights, lowestPrice];
}


export const ResultCalendar = React.memo(({daySelected }: props) => {
    const results = useSelector((state: State) => state.results)
    const month = useSelector((state: State) => state.form.startDate)
    const dispatch = useDispatch();
    let cheapestFrom = 999;
    let cheapestReturn = 999;
    const calendarWeeks = (() => {
        const startingDay = new Date(month.year, month.month, 1);
        const startingCalendarDay = new Date(month.year, month.month, 1 - startingDay.getDay())
        const endingDay = new Date(month.year, month.month + 1, 0);
        const endingCalendarDay = new Date(month.year, month.month, endingDay.getDate() + 6 - endingDay.getDay());
        const weekCount = Math.round(Math.round((endingCalendarDay.getTime() - startingCalendarDay.getTime()) / oneDay) / 7);
        return Array.from({ length: weekCount })
            .map((_, i) => Array.from({ length: 7 })
                .map((_, j) => {
                    const date = new Date(startingCalendarDay.getFullYear(), startingCalendarDay.getMonth(), startingCalendarDay.getDate() + (i * 7 + j))
                    const dateFormated = formatDate(date);
                    const [flights, lowestPrice] = getDayFlights(results.from, dateFormated);
                    const [flightsReturn, lowestPriceReturn] = getDayFlights(results.return, dateFormated);
                    cheapestReturn = parseInt(lowestPriceReturn) && parseInt(lowestPriceReturn) < cheapestReturn ? parseInt(lowestPriceReturn) : cheapestReturn;
                    cheapestFrom = parseInt(lowestPrice) && parseInt(lowestPrice) < cheapestFrom ? parseInt(lowestPrice) : cheapestFrom;
                    return {
                        flights,
                        date,
                        dateFormated,
                        lowestPrice,
                        flightsReturn,
                        lowestPriceReturn,
                        disabled: date < startingDay || date > endingDay,
                    }
                }))})();
    const swipeCalendar = useDrag(({ swipe }) => {
        if (swipe && swipe[0] === 1 && month.value ) {
            dispatch(updateForm('startDate', MONTHS[month.value - 1]));
            dispatch(submit());
        } else if (swipe && swipe[0] === -1 && month.value < MONTHS.length - 1) {
            dispatch(updateForm('startDate', MONTHS[month.value + 1]));
            dispatch(submit());
        }
    }, [month])

    return (
        <div className="calendar__container" id="calendar" {...swipeCalendar()}>
            <div className="calendar__header">
                <Button
                    onClick={() => dispatch(updateForm('startDate', MONTHS[month.value - 1])) && dispatch(submit())}
                    disabled={!month.value}
                    className="calendar__header-btn">{'<< Prev'}</Button>
                <div className="calendar__header-title">{month.text}</div>
                <Button
                    onClick={() => dispatch(updateForm('startDate', MONTHS[month.value + 1])) && dispatch(submit())}
                    disabled={month.value === MONTHS.length - 1}
                    className="calendar__header-btn">{'Next >>'}</Button>
            </div>
            <div className="loading-shade__container">
                {results.loading ? <div className="loading-shade">Loading...</div> : null}
                <table className={`calendar ${results.loading ? 'filter-blur' : ''}`}>
                    <thead>
                        <tr>
                            {days.map(day => <th className="calendar__table-header" key={day}>{day}</th>)}
                        </tr>
                    </thead>
                    <tbody>
                        {calendarWeeks.map((week, i) =>
                            <tr className="calendar__row" key={i}>
                                {week.map((day, j) =>
                                    <td key={j} className="calendar__table-data" onClick={() => !day.disabled && daySelected(day)}>
                                        <div className={`calendar__day noselect ${day.disabled ? 'calendar__day--disabled' : ''}`}>
                                            <div className="calendar__day-header">
                                                <div className="calendar__day-number">{day.date.getDate()}</div>
                                                {!day.disabled && parseInt(day.lowestPrice) <= cheapestFrom + 10 && <StarBorderIcon className="icon-star" />}
                                            </div>
                                            <div className="calendar__day-flights calendar__day-flights--from">
                                                {!day.disabled && day.lowestPrice}
                                            </div>
                                            {!day.disabled && (day.lowestPriceReturn || day.lowestPrice) && <div className="calendar__day-line"></div>}
                                            <div className="calendar__day-flights calendar__day-flights--return">
                                                {!day.disabled && day.lowestPriceReturn}
                                            </div>
                                            <div className="calendar__day-footer">
                                                {!day.disabled && parseInt(day.lowestPriceReturn) <= cheapestReturn + 10 && <StarBorderIcon className="icon-star-return"/>}
                                                <div className="calendar__day-count">({day.flights.length},{day.flightsReturn.length})</div>
                                            </div>
                                        </div>
                                    </td>)}
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        </div>
    );
})

export default ResultCalendar;
