import React, { useState } from 'react';
import { Paper } from '@material-ui/core';
import ResultCalendar from './ResultCalendar/ResultCalendar';
import ResultList from './ResultList/ResultList';
import { Day } from '../../../shared/interfaces';
import './Results.css';

const Results = React.memo(() => {
    const [selectedDay, setSelectedDay] = useState<Day>({
        flights: [],
        flightsReturn: [],
        date: new Date(),
        dateFormated: 'Select Date',
        disabled: true,
        lowestPrice: '',
        lowestPriceReturn: '',
    })
    return (
        <div className="result-list-container">
            <Paper className="result-list">
                <React.Fragment>
                    <ResultCalendar daySelected={setSelectedDay}/>
                    <div className="result-list__day-title">{selectedDay.dateFormated}</div>
                    <div className="result-list__tables">
                        <ResultList
                            results={selectedDay.flights}
                            label="Destination flights"/>
                        <ResultList
                            results={selectedDay.flightsReturn}
                            label="Return flights"/>
                    </div>
                </React.Fragment>
            </Paper>
        </div>
    )
})

export default Results;
