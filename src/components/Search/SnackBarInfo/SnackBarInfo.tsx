import React, { useEffect, useCallback } from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import Slide from '@material-ui/core/Slide';
import { TransitionProps } from '@material-ui/core/transitions';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../../shared/interfaces';
import { closeNotification } from '../../../store/infoActions';
import useDebounce from '../../../shared/useDebounce';
import CloseIcon from '@material-ui/icons/Close';
import { IconButton } from '@material-ui/core';

function SlideTransition(props: TransitionProps) {
    return <Slide {...props} direction="up" />;
}

export default function SnackBarInfo() {
    const {showInfo, info} = useSelector((state: State) => state.info)
    const dispatch = useDispatch();

    const close = useDebounce(showInfo, 4000);

    const handleClose = useCallback(
        () => {
            dispatch(closeNotification());
        },
        [dispatch],
    )


    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect( () => {handleClose()}, [close])


    return (
        <div>
            <Snackbar
                key={info}
                open={showInfo}
                onClose={handleClose}
                TransitionComponent={SlideTransition}
                message={info}
                action={
                    <IconButton
                        color="secondary"
                        aria-label="close"
                        onClick={handleClose}>
                        <CloseIcon />
                    </IconButton>
                }
            />
        </div>
    );
}
