import React from 'react';
import './LoadingBar.css';
import { useSelector } from 'react-redux';
import { State } from '../../../shared/interfaces';
import { LinearProgress } from '@material-ui/core';


export const LoadingBar = () => {
    const results = useSelector((state: State) => state.results);
    return (results.done === results.total ? null :
        <div className="loading-bar__container">
            <div className="loading-bar">
                <LinearProgress variant="determinate" color="secondary" value={results.total ? Math.floor(results.done/results.total*100) : 100} />
            </div>
        </div>);
}

export default LoadingBar;
