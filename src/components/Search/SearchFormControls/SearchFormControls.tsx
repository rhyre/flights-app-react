import React, { useCallback } from 'react';
import { Button, Paper, Divider } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../../shared/interfaces';
import MonthSelect from './MonthSelect/MonthSelect';
import { updateForm, removeAirport, clearAirports } from '../../../store/formActions';
import { submit } from '../../../store/resultsActions';
import './SearchFormControls.css';
import AirportsSelected from './AirportsSelected/AirportsSelected';
import { MONTHS } from '../../../shared/data';

const SearchFormControls = () => {
    const formControls = useSelector((state: State) => state.form)
    const dispatch = useDispatch();
    const onRemoveAirport = useCallback((selector: string, airport: number) => dispatch(removeAirport(selector, airport)), [dispatch])
    const onClearAirports = useCallback((selector: string, airport: number) => dispatch(clearAirports(selector)), [dispatch])
    return (
        <Paper className="form-data__container">
            <div className="form-data__form">
                <MonthSelect
                    label="Month select"
                    selectedMonth={(value: number) => dispatch(updateForm('startDate', MONTHS[value]))}
                    value={formControls.startDate.value}></MonthSelect>
            </div>
            <Divider light />
            <AirportsSelected
                airports={formControls.fromAirports}
                selector='fromAirports'
                label="Outbound"
                onRemoveAirport={onRemoveAirport}
                onClickClear={onClearAirports} />
            <Divider light />
            <AirportsSelected
                airports={formControls.destinationAirports}
                selector='destinationAirports'
                label="Destination"
                onRemoveAirport={onRemoveAirport}
                onClickClear={onClearAirports} />
            <Button
                className="form-data__submit-btn"
                disabled={!(formControls.fromAirports.length && formControls.destinationAirports.length)}
                variant="contained"
                color="primary"
                onClick={() => dispatch(submit())}>Search flights</Button>
        </Paper>

    )
}

export default SearchFormControls;
