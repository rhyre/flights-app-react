import React, { useState, useCallback, useEffect } from 'react';
import { Input, Button, List, ListItem, ListItemText, ListItemSecondaryAction, IconButton, InputAdornment, Dialog, DialogTitle } from '@material-ui/core';
import { SelectOption } from '../../../../shared/interfaces';
import './ModalSelect.css'
import Clear from '@material-ui/icons/Clear';
import useDebounce from '../../../../shared/useDebounce';


interface ModalSelectDialogProps {
    open: boolean;
    onClose: () => void;
    data: SelectOption[];
    label: string;
    header: string;
    onClickAdd: any;
}

const ModalSelectDialog = ({ onClose, open, data, label, header, onClickAdd }: ModalSelectDialogProps) => {
    const [dataFiltered, setDataFiltered] = useState(data.slice(0, 30));
    const [query, setQuery] = useState('');
    const debounceQuery = useDebounce(query, 200);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => setDataFiltered(data.filter(obj => obj.text.toLowerCase().includes(query.toLowerCase())).slice(0, 30)), [debounceQuery]);
    const onClearInput = useCallback(() => setQuery(''), []);
    const onChangeFilter = useCallback((ev) => setQuery(ev.target.value), []);
    return (
        <Dialog onClose={onClose} open={open} fullWidth>
            <div className="modal-select__container">
                <DialogTitle className="modal-select__header">
                    <div className="modal-select__header-content">
                        <span>{header} {label}</span>
                        <IconButton size="small" onClick={onClose} color="primary" component="span">
                            <Clear />
                        </IconButton>
                    </div>
                </DialogTitle>
                <Input
                    value={query}
                    placeholder="Search..."
                    autoFocus
                    className="modal-select__input"
                    endAdornment={query &&
                        <InputAdornment position="end">
                            <IconButton
                                size="small"
                                onClick={onClearInput}>
                                <Clear />
                            </IconButton>
                        </InputAdornment>
                    }
                    onChange={onChangeFilter} />
                <div className="modal-select__list">
                    <List dense>
                        {dataFiltered.map(value => {
                            return (
                                <ListItem key={value.text} button className="modal-select__list-item">
                                    <ListItemText id={value.text} primary={value.name || value.text} secondary={value.id || null} />
                                    <ListItemSecondaryAction>
                                        <Button color="primary" onClick={() => onClickAdd(value)} onMouseDown={(ev) => ev.preventDefault()}>Add</Button>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            );
                        })}
                    </List>
                </div>
            </div>
        </Dialog>
    );
}

interface props { data: SelectOption[], onClickAdd: any, label: string, header: string };

const ModalSelect = ({ data, onClickAdd, label, header }: props) => {
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = useCallback(() => setOpen(true), [])
    const handleClose = useCallback(() => setOpen(false), [])

    return (
        <React.Fragment>
            <Button color="primary" onClick={handleClickOpen}>{label}</Button>
            <ModalSelectDialog open={open} onClose={handleClose} data={data} label={label} header={header} onClickAdd={onClickAdd} />
        </React.Fragment>
    )
}

export default React.memo(ModalSelect);
