import React from 'react';
import { TextField } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { MONTHS } from '../../../../shared/data';

interface props { label: string, selectedMonth: any, value: number, minMonth?: number };

export const MonthSelect = ({ label, selectedMonth, value, minMonth }: props) =>
    <Autocomplete
        options={MONTHS.slice()}
        value={MONTHS[value]}
        onChange={(ev: any, value: any) => selectedMonth(value ? value.value : 0)}
        getOptionLabel={option => option.text}
        renderInput={params => (
            <TextField {...params} label={label} fullWidth />
        )}
    ></Autocomplete>

export default MonthSelect;
