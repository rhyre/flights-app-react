import React, { useCallback } from 'react';
import { Airport, SelectOption, State } from '../../../../shared/interfaces';
import { List, ListItem, ListItemText, ListItemSecondaryAction, IconButton, Button } from '@material-ui/core';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { useDispatch, useSelector} from 'react-redux';
import { addAirport, addCountry } from '../../../../store/formActions';
import ModalSelect from '../ModalSelect/ModalSelect';
import DeleteIcon from '@material-ui/icons/Delete';
import './AirportsSelected.css';
import { COUNTRIES_SELECT } from '../../../../shared/data';


interface props { airports: Airport[], onRemoveAirport: any, label: string, onClickClear: any, selector: string };

export const AirportsSelected = React.memo(({ airports, onRemoveAirport, label, onClickClear, selector }: props) => {
    const airportsList = useSelector((state: State) => state.routes.airports)
    const airportsSelect: SelectOption[] = airportsList.map(obj => ({
        name: obj.name,
        id: obj.id,
        text: `${obj.name} [${obj.id}]`
    }))
    const dispatch = useDispatch();
    const onAddAirport = useCallback((airport: Airport) => dispatch(addAirport(selector, airport)), [dispatch, selector])
    const onAddCountry = useCallback((country: SelectOption) => dispatch(addCountry(selector, country.id)), [dispatch, selector])
    return (
        <div className="selcted-airports">
            <div className="selected-airports__title">{label} airports</div>
            <div className="selected-airports__actions">
                <ModalSelect data={airportsSelect} onClickAdd={onAddAirport} label="Airports" header={label}/>
                <ModalSelect data={COUNTRIES_SELECT} onClickAdd={onAddCountry} label="Countries" header={label}/>
                <Button className="clear-btn" color="secondary" onClick={() => onClickClear(selector)}>Clear</Button>
            </div>
            <div className="selected-airports__list">
                <List component="nav">
                    <TransitionGroup>
                        {airports.map((airport: Airport, index: number) => {
                            return (
                                <CSSTransition key={airport.id} classNames="fade" timeout={300}>
                                    <ListItem button className="selected-airports__list-item" >
                                        <ListItemText primary={`${airport.name} [${airport.id}]`}/>
                                        <ListItemSecondaryAction>
                                            <IconButton edge="end" aria-label="delete" onClick={() => onRemoveAirport(selector, index)}>
                                                <DeleteIcon></DeleteIcon>
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                </CSSTransition>
                            )
                        })}
                    </TransitionGroup>
                </List>
            </div>
        </div>)}
);

export default AirportsSelected;
