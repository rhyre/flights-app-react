import React, { useEffect } from 'react';
import './App.css';
import Home from './components/Search/Home';

const App: React.FC = () => {
  useEffect(() => {
    document.title = "Flight search";
    (document.getElementsByTagName("META")[3] as any).content = 'WizzAir and RyanAir easy search by countries';
  }, []);
  return (
      <Home/>
  );
}

export default App;
